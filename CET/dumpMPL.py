import matplotlib.pyplot as plt
import numpy as np

def export_cmap_to_cpt(name, vmin=0,vmax=1, N=255, filename="test.txt"):
    cmap = plt.get_cmap(name,255)
    cols = (cmap(np.linspace(0.,1.,N))[:,:3]*255).astype(int)
    vals = np.linspace(vmin,vmax,N)
    arr = np.c_[vals[:-1],cols[:-1]]
    # save to file
    fmt = "%e %3d %3d %3d"
    np.savetxt(filename, arr, fmt=fmt,
               header="# " + name,
               footer = "", comments="")

export_cmap_to_cpt("inferno")
