import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm

from collections import OrderedDict

from lxml import etree

def export_cmap_to_cpt(name, vmin=0,vmax=1, N=255, filename="test.txt"):
    cmap = plt.get_cmap(name,255)
    cols = (cmap(np.linspace(0.,1.,N))[:,:3]*255).astype(int)
    vals = np.linspace(vmin,vmax,N)
    arr = np.c_[vals[:-1],cols[:-1]]
    # save to file
    fmt = "%e %3d %3d %3d"
    np.savetxt(filename, arr, fmt=fmt,
               header="# " + name,
               footer = "", comments="")



cmaps = plt.colormaps()



# create XML 
#root = etree.Element('ColorMaps', mpl_version=mpl.__version__)
root = etree.Element('ColorMaps')



N=255
vmin=0
vmax=1

for pal in cmaps:
    cm = etree.Element('ColorMap', name=pal, space="RGB")

    cmap = plt.get_cmap(pal,255)
    ###cols = (cmap(np.linspace(0.,1.,N))[:,:3]*255).astype(int)
    cols = cmap(np.linspace(0.,1.,N))[:,:3]
    vals = np.linspace(vmin,vmax,N)
    arr = np.c_[vals[:-1],cols[:-1]]

    for row in arr:
        etree.SubElement(cm, 'Point', x='{:.6f}'.format(row[0]), o='{:.6f}'.format(row[0]), r='{:.6f}'.format(row[1]), g='{:.6f}'.format(row[2]), b='{:.6f}'.format(row[3]))
    
    root.append(cm)
        
# pretty string
s = etree.tostring(root, pretty_print=True)
#print(s.decode("utf-8"))

text_file = open("All_mpl"+mpl.__version__+"_cmaps.xml", "w")
text_file.write(s.decode("utf-8"))
text_file.close()

