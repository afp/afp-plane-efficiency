# AFP plane efficiency

## How to use

First you need to generate the class for your ntuple using something like:

```root -q run3/20240530_475369HLT_j15f_L1RD0_FILLED_HVscan.root -e "TreeHits->MakeClass()"```

Then you can run with:

```root -x plane_efficiency.C++(<run-number>)```

## Notes
 - inefficient: runs a single plane at a time (useful for HV scan, when each plane is tested in a separate lumi block range)