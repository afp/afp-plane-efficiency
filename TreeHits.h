//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Jun 19 13:30:15 2024 by ROOT version 6.28/04
// from TTree TreeHits/TreeHits
// found on file: run3/20240530_475369HLT_j15f_L1RD0_FILLED_HVscan.root
//////////////////////////////////////////////////////////

#ifndef TreeHits_h
#define TreeHits_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"

class TreeHits {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           evN;
   Int_t           lmiBl;
   Float_t         mu;
   UInt_t          timestamp;
   UInt_t          bcid;
   vector<bool>    *Trigger_L1_beforePrescale;
   vector<bool>    *Trigger_L1_afterPrescale;
   vector<bool>    *Trigger_HLT_isPrescaledOut;
   vector<bool>    *Trigger_HLT_afterPrescale;
   Int_t           NVertex;
   Int_t           NSecVertex;
   Int_t           NInDetTrackParticles;
   Int_t           nhits[4][4];
   Int_t           nclusters_toolbox[4][4];
   Int_t           ntracks_toolbox[4];
   Int_t           nprotons_toolbox[2];
   Int_t           NAfpToolboxProton;
   vector<int>     *AfpToolboxProton_Id;
   vector<int>     *AfpToolboxProton_Side;
   vector<float>   *AfpToolboxProton_Pt;
   vector<float>   *AfpToolboxProton_Px;
   vector<float>   *AfpToolboxProton_Py;
   vector<float>   *AfpToolboxProton_Pz;
   vector<float>   *AfpToolboxProton_E;
   vector<float>   *AfpToolboxProton_Eta;
   vector<float>   *AfpToolboxProton_Phi;
   vector<float>   *AfpToolboxProton_ChiSq;
   Int_t           NAfpToolboxTrack;
   vector<int>     *AfpToolboxTrack_Id;
   vector<int>     *AfpToolboxTrack_Station;
   vector<float>   *AfpToolboxTrack_X;
   vector<float>   *AfpToolboxTrack_Y;
   vector<float>   *AfpToolboxTrack_SX;
   vector<float>   *AfpToolboxTrack_SY;
   Int_t           NAfpToolboxCluster;
   vector<int>     *AfpToolboxCluster_Id;
   vector<int>     *AfpToolboxCluster_Station;
   vector<int>     *AfpToolboxCluster_Plane;
   vector<float>   *AfpToolboxCluster_X;
   vector<float>   *AfpToolboxCluster_Y;
   vector<float>   *AfpToolboxCluster_Z;
   vector<float>   *AfpToolboxCluster_Q;
   vector<int>     *AfpToolboxCluster_N;

   // List of branches
   TBranch        *b_evN;   //!
   TBranch        *b_lmiBl;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_timestamp;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_Trigger_L1_beforePrescale;   //!
   TBranch        *b_Trigger_L1_afterPrescale;   //!
   TBranch        *b_Trigger_HLT_isPrescaledOut;   //!
   TBranch        *b_Trigger_HLT_afterPrescale;   //!
   TBranch        *b_NVertex;   //!
   TBranch        *b_NSecVertex;   //!
   TBranch        *b_NInDetTrackParticles;   //!
   TBranch        *b_nhits;   //!
   TBranch        *b_nclusters_toolbox;   //!
   TBranch        *b_ntracks_toolbox;   //!
   TBranch        *b_nprotons_toolbox;   //!
   TBranch        *b_NAfpToolboxProton;   //!
   TBranch        *b_AfpToolboxProton_Id;   //!
   TBranch        *b_AfpToolboxProton_Side;   //!
   TBranch        *b_AfpToolboxProton_Pt;   //!
   TBranch        *b_AfpToolboxProton_Px;   //!
   TBranch        *b_AfpToolboxProton_Py;   //!
   TBranch        *b_AfpToolboxProton_Pz;   //!
   TBranch        *b_AfpToolboxProton_E;   //!
   TBranch        *b_AfpToolboxProton_Eta;   //!
   TBranch        *b_AfpToolboxProton_Phi;   //!
   TBranch        *b_AfpToolboxProton_ChiSq;   //!
   TBranch        *b_NAfpToolboxTrack;   //!
   TBranch        *b_AfpToolboxTrack_Id;   //!
   TBranch        *b_AfpToolboxTrack_Station;   //!
   TBranch        *b_AfpToolboxTrack_X;   //!
   TBranch        *b_AfpToolboxTrack_Y;   //!
   TBranch        *b_AfpToolboxTrack_SX;   //!
   TBranch        *b_AfpToolboxTrack_SY;   //!
   TBranch        *b_NAfpToolboxCluster;   //!
   TBranch        *b_AfpToolboxCluster_Id;   //!
   TBranch        *b_AfpToolboxCluster_Station;   //!
   TBranch        *b_AfpToolboxCluster_Plane;   //!
   TBranch        *b_AfpToolboxCluster_X;   //!
   TBranch        *b_AfpToolboxCluster_Y;   //!
   TBranch        *b_AfpToolboxCluster_Z;   //!
   TBranch        *b_AfpToolboxCluster_Q;   //!
   TBranch        *b_AfpToolboxCluster_N;   //!

   TreeHits(TTree *tree=0);
   virtual ~TreeHits();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef TreeHits_cxx
TreeHits::TreeHits(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("run3/20240530_475369HLT_j15f_L1RD0_FILLED_HVscan.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("run3/20240530_475369HLT_j15f_L1RD0_FILLED_HVscan.root");
      }
      f->GetObject("TreeHits",tree);

   }
   Init(tree);
}

TreeHits::~TreeHits()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t TreeHits::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t TreeHits::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void TreeHits::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   Trigger_L1_beforePrescale = 0;
   Trigger_L1_afterPrescale = 0;
   Trigger_HLT_isPrescaledOut = 0;
   Trigger_HLT_afterPrescale = 0;
   AfpToolboxProton_Id = 0;
   AfpToolboxProton_Side = 0;
   AfpToolboxProton_Pt = 0;
   AfpToolboxProton_Px = 0;
   AfpToolboxProton_Py = 0;
   AfpToolboxProton_Pz = 0;
   AfpToolboxProton_E = 0;
   AfpToolboxProton_Eta = 0;
   AfpToolboxProton_Phi = 0;
   AfpToolboxProton_ChiSq = 0;
   AfpToolboxTrack_Id = 0;
   AfpToolboxTrack_Station = 0;
   AfpToolboxTrack_X = 0;
   AfpToolboxTrack_Y = 0;
   AfpToolboxTrack_SX = 0;
   AfpToolboxTrack_SY = 0;
   AfpToolboxCluster_Id = 0;
   AfpToolboxCluster_Station = 0;
   AfpToolboxCluster_Plane = 0;
   AfpToolboxCluster_X = 0;
   AfpToolboxCluster_Y = 0;
   AfpToolboxCluster_Z = 0;
   AfpToolboxCluster_Q = 0;
   AfpToolboxCluster_N = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("evN", &evN, &b_evN);
   fChain->SetBranchAddress("lmiBl", &lmiBl, &b_lmiBl);
   fChain->SetBranchAddress("mu", &mu, &b_mu);
   fChain->SetBranchAddress("timestamp", &timestamp, &b_timestamp);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("Trigger_L1_beforePrescale", &Trigger_L1_beforePrescale, &b_Trigger_L1_beforePrescale);
   fChain->SetBranchAddress("Trigger_L1_afterPrescale", &Trigger_L1_afterPrescale, &b_Trigger_L1_afterPrescale);
   fChain->SetBranchAddress("Trigger_HLT_isPrescaledOut", &Trigger_HLT_isPrescaledOut, &b_Trigger_HLT_isPrescaledOut);
   fChain->SetBranchAddress("Trigger_HLT_afterPrescale", &Trigger_HLT_afterPrescale, &b_Trigger_HLT_afterPrescale);
   fChain->SetBranchAddress("NVertex", &NVertex, &b_NVertex);
   fChain->SetBranchAddress("NSecVertex", &NSecVertex, &b_NSecVertex);
   fChain->SetBranchAddress("NInDetTrackParticles", &NInDetTrackParticles, &b_NInDetTrackParticles);
   fChain->SetBranchAddress("nhits", nhits, &b_nhits);
   fChain->SetBranchAddress("nclusters_toolbox", nclusters_toolbox, &b_nclusters_toolbox);
   fChain->SetBranchAddress("ntracks_toolbox", ntracks_toolbox, &b_ntracks_toolbox);
   fChain->SetBranchAddress("nprotons_toolbox", nprotons_toolbox, &b_nprotons_toolbox);
   fChain->SetBranchAddress("NAfpToolboxProton", &NAfpToolboxProton, &b_NAfpToolboxProton);
   fChain->SetBranchAddress("AfpToolboxProton_Id", &AfpToolboxProton_Id, &b_AfpToolboxProton_Id);
   fChain->SetBranchAddress("AfpToolboxProton_Side", &AfpToolboxProton_Side, &b_AfpToolboxProton_Side);
   fChain->SetBranchAddress("AfpToolboxProton_Pt", &AfpToolboxProton_Pt, &b_AfpToolboxProton_Pt);
   fChain->SetBranchAddress("AfpToolboxProton_Px", &AfpToolboxProton_Px, &b_AfpToolboxProton_Px);
   fChain->SetBranchAddress("AfpToolboxProton_Py", &AfpToolboxProton_Py, &b_AfpToolboxProton_Py);
   fChain->SetBranchAddress("AfpToolboxProton_Pz", &AfpToolboxProton_Pz, &b_AfpToolboxProton_Pz);
   fChain->SetBranchAddress("AfpToolboxProton_E", &AfpToolboxProton_E, &b_AfpToolboxProton_E);
   fChain->SetBranchAddress("AfpToolboxProton_Eta", &AfpToolboxProton_Eta, &b_AfpToolboxProton_Eta);
   fChain->SetBranchAddress("AfpToolboxProton_Phi", &AfpToolboxProton_Phi, &b_AfpToolboxProton_Phi);
   fChain->SetBranchAddress("AfpToolboxProton_ChiSq", &AfpToolboxProton_ChiSq, &b_AfpToolboxProton_ChiSq);
   fChain->SetBranchAddress("NAfpToolboxTrack", &NAfpToolboxTrack, &b_NAfpToolboxTrack);
   fChain->SetBranchAddress("AfpToolboxTrack_Id", &AfpToolboxTrack_Id, &b_AfpToolboxTrack_Id);
   fChain->SetBranchAddress("AfpToolboxTrack_Station", &AfpToolboxTrack_Station, &b_AfpToolboxTrack_Station);
   fChain->SetBranchAddress("AfpToolboxTrack_X", &AfpToolboxTrack_X, &b_AfpToolboxTrack_X);
   fChain->SetBranchAddress("AfpToolboxTrack_Y", &AfpToolboxTrack_Y, &b_AfpToolboxTrack_Y);
   fChain->SetBranchAddress("AfpToolboxTrack_SX", &AfpToolboxTrack_SX, &b_AfpToolboxTrack_SX);
   fChain->SetBranchAddress("AfpToolboxTrack_SY", &AfpToolboxTrack_SY, &b_AfpToolboxTrack_SY);
   fChain->SetBranchAddress("NAfpToolboxCluster", &NAfpToolboxCluster, &b_NAfpToolboxCluster);
   fChain->SetBranchAddress("AfpToolboxCluster_Id", &AfpToolboxCluster_Id, &b_AfpToolboxCluster_Id);
   fChain->SetBranchAddress("AfpToolboxCluster_Station", &AfpToolboxCluster_Station, &b_AfpToolboxCluster_Station);
   fChain->SetBranchAddress("AfpToolboxCluster_Plane", &AfpToolboxCluster_Plane, &b_AfpToolboxCluster_Plane);
   fChain->SetBranchAddress("AfpToolboxCluster_X", &AfpToolboxCluster_X, &b_AfpToolboxCluster_X);
   fChain->SetBranchAddress("AfpToolboxCluster_Y", &AfpToolboxCluster_Y, &b_AfpToolboxCluster_Y);
   fChain->SetBranchAddress("AfpToolboxCluster_Z", &AfpToolboxCluster_Z, &b_AfpToolboxCluster_Z);
   fChain->SetBranchAddress("AfpToolboxCluster_Q", &AfpToolboxCluster_Q, &b_AfpToolboxCluster_Q);
   fChain->SetBranchAddress("AfpToolboxCluster_N", &AfpToolboxCluster_N, &b_AfpToolboxCluster_N);
   Notify();
}

Bool_t TreeHits::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void TreeHits::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t TreeHits::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef TreeHits_cxx
