#include <TStyle.h>
#include <TColor.h>
#include <iostream>
#include <TROOT.h>
#include <TLatex.h>
#include <TImage.h>
#include <TFrame.h>

#include <TColor.h>
#include <TSystem.h>

#include <boost/exception/diagnostic_information.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/trim.hpp>

using namespace std;
using boost::property_tree::ptree;
using boost::algorithm::trim_copy;

/*!
 *  \addtogroup ShinyStyle
 *  @{
 */

/** @brief Main ROOT TStyle automated settings for NA61/SHINE

    This namespace provides a unified style for data visualization in NA61/SHINE.
    Created to automatically make your plots nice and shiny.

    @author Maciej Lewicki
    @date Sep 2020
*/

namespace ShinyStyle
{

	/** List of colours for different reactions (p+p, Be+Be, Ar+Sc, Xe+La, Pb+Pb) recorded by NA61/SHINE
        */
	std::map<std::string,int> gReactionColor;

	/** List of marker styles for different reactions  (p+p, Be+Be, Ar+Sc, Xe+La, Pb+Pb)recorded by NA61/SHINE
        */
        std::map<std::string,int> gReactionMarker;

	/** List of colours for 2 categories
        */
	int gColors2[] = {kRed+1, kBlue+1};
	/** List of colours for 3 categories
        */
	int gColors3[] = {kRed+1, kGreen+2, kBlue+1};
	/** List of colours for 4 categories
        */
	int gColors4[]={kRed+1,kYellow+1,kGreen+2,kBlue+1};
	/** List of colours for 5 categories
        */
	int gColors5[]={kRed+1,kYellow+1,kGreen+2,kAzure+1,kBlue+1};
	/** List of colours for 6 categories
        */
	int gColors6[]={kRed+1,kOrange+7,kYellow+1,kGreen+2,kAzure+1,kBlue+1};

	/** List of lighter colours for 2 categories
        */
	int gColorsLight2[] = {kRed-7, kBlue-7};
	/** List of lighter colours for 3 categories
        */
	int gColorsLight3[] = {kRed-7, kGreen-7, kBlue-7};
	/** List of lighter colours for 4 categories
        */
	int gColorsLight4[]={kRed-7,kYellow-0,kGreen-7,kBlue-7};
	/** List of lighter colours for 5 categories
        */
	int gColorsLight5[]={kRed-7,kYellow-0,kGreen-7,kAzure+6,kBlue-7};
	/** List of lighter colours for 6 categories
        */
	int gColorsLight6[]={kRed-7,kOrange+1,kYellow-0,kGreen-7,kAzure+6,kBlue-7};

	/** List of darker colours for 2 categories
        */
	int gColorsDark2[] = {kRed+2, kBlue+2};
	/** List of darker colours for 3 categories
        */
	int gColorsDark3[] = {kRed+2, kGreen+3, kBlue+2};
	/** List of darker colours for 4 categories
        */
	int gColorsDark4[]={kRed+2,kYellow+3,kGreen+3,kBlue+2};
	/** List of darker colours for 5 categories
        */
	int gColorsDark5[]={kRed+2,kYellow+3,kGreen+3,kAzure+4,kBlue+2};
	/** List of darker colours for 6 categories
        */
	int gColorsDark6[]={kRed+2,kOrange+4,kYellow+3,kGreen+3,kAzure+4,kBlue+2};

	/** List of marker styles for 2 categories
        */
	const int gMarkers2[] = {kFullSquare,kFullCircle};
	/** List of marker styles for 3 categories
        */
	const int gMarkers3[] = {kFullSquare,kFullCross,kFullCircle};
	/** List of marker styles for 4 categories
        */
	const int gMarkers4[] = {kFullSquare,kFullTriangleUp,kFullTriangleDown,kFullCircle};
	/** List of marker styles for 5 categories
        */
	const int gMarkers5[] = {kFullSquare,kFullTriangleUp,kFullCross,kFullTriangleDown,kFullCircle};
	/** List of marker styles for 6 categories
        */
	const int gMarkers6[] = {kFullSquare,kFullTriangleUp,kFullCross,kFullDoubleDiamond,kFullTriangleDown,kFullCircle};

	/** List of open markers styles for 2 categories
        */
	const int gMarkersOpen2[] = {kOpenSquare,kOpenCircle};
	/** List of open markers styles for 3 categories
        */
	const int gMarkersOpen3[] = {kOpenSquare,kOpenCross,kOpenCircle};
	/** List of open markers styles for 4 categories
        */
	const int gMarkersOpen4[] = {kOpenSquare,kOpenTriangleUp,kOpenTriangleDown,kOpenCircle};
	/** List of open markers styles for 5 categories
        */
	const int gMarkersOpen5[] = {kOpenSquare,kOpenTriangleUp,kOpenCross,kOpenTriangleDown,kOpenCircle};
	/** List of open markers styles for 6 categories
        */
	const int gMarkersOpen6[] = {kOpenSquare,kOpenTriangleUp,kOpenCross,kOpenDoubleDiamond,kOpenTriangleDown,kOpenCircle};

	/** Dictionary containing scales for each of the marker style to correct its size to have similar visual impact
        */
	std::map<int,float> gMarkerSizes;

	/** Map of matplotlib tab10 (tableu default) palette
        */
	std::map<int,int> gColorsTab10;

	/** Base Font Style (ROOT code)
	*/
	float gBaseFont;

	/** Base Text Size
	*/
	float gBaseTextSize;



	/** Change text size globally
	*/
	void SetBaseTextSize(double BaseTextSize=20.) {
		gBaseTextSize=BaseTextSize;
		gStyle->SetTitleSize(gBaseTextSize, "xy");
		gStyle->SetLabelSize(0.8*gBaseTextSize, "xy");
		gStyle->SetTitleSize(gBaseTextSize*0.95, "z");
		gStyle->SetLabelSize(0.75*gBaseTextSize, "z");
		gStyle->SetTextSize(gBaseTextSize);

		// not sure if that works
		gStyle->SetTitleOffset(20./gBaseTextSize,"x");
		gStyle->SetTitleOffset(20./gBaseTextSize,"y");
		gStyle->SetTitleOffset(20./gBaseTextSize,"z");
		// requires tuning (if that's even possible)
	}

	/**
	After matplotlib: category10 color palette used by Vega and d3 originally developed at Tableau
	*/
	void SetColorsTab10() {
		string mpl_hex[10]={"#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b", "#e377c2", "#7f7f7f", "#bcbd22", "#17becf"};
		for(int i=0; i<10; i++) {
			gColorsTab10[i]=TColor::GetColor(mpl_hex[i].c_str());
		}
	}


	/** Base function for setting basic style properties
	*/
	void SetNA61Style(int _font=133) {

		gSystem->Setenv("SHINYDIR","");

		gBaseFont = _font;

		const Style_t font = _font;	// fonts: 132 = serif, 62 = default
		//const Style_t font = 43;	// fonts: 132 = serif, 62 = default
						// 133,63 = fonts with size in px, independent of the pad size
		const double BASE_TEXT_SIZE = 20;

		gStyle->Reset();

		gStyle->SetCanvasDefH(550);
		gStyle->SetCanvasDefW(650);

		gStyle->SetTitleFont(font, "xy");
		gStyle->SetTitleSize(BASE_TEXT_SIZE, "xy");
		gStyle->SetLabelFont(font, "xy");
		gStyle->SetLabelSize(0.8*BASE_TEXT_SIZE, "xy");

		// for some mysterious reason z axis text is fatter O_o
		gStyle->SetTitleFont(font, "z");
		gStyle->SetTitleSize(BASE_TEXT_SIZE*0.95, "z");
		gStyle->SetLabelFont(font, "z");
		gStyle->SetLabelSize(0.75*BASE_TEXT_SIZE, "z");

		//gStyle->SetTextFont(font);
		gStyle->SetTextSize(BASE_TEXT_SIZE);
		gStyle->SetLegendFont(font);

		//dimensions
		gStyle->SetPadTopMargin(0.1);
		gStyle->SetPadBottomMargin(0.15);
		gStyle->SetPadLeftMargin(0.2);
		gStyle->SetPadRightMargin(0.2);

		//make all white:
		gStyle->SetCanvasColor(0);
		gStyle->SetPadColor(kWhite);
		gStyle->SetFrameFillColor(0);
		gStyle->SetFrameLineColor(kBlack);
		gStyle->SetFrameBorderMode(0);
		gStyle->SetCanvasBorderMode(0);
		gStyle->SetPadBorderMode(0);


		//statbox
		gStyle->SetOptStat(0);
		gStyle->SetOptFit(0);

		//label
		gStyle->SetLegendBorderSize(1);
		gStyle->SetLegendFillColor(0); //does nothing?!
		gStyle->SetFillStyle(0);

		gStyle->SetNdivisions(505);

		//title
		//  -this:
		gStyle->SetOptTitle(0);
		//  -or this:
		/*gStyle->SetTitleBorderSize(0);
		gStyle->SetTitleX(0.2);
		gStyle->SetTitleY(0.975f);
		gStyle->SetTitleH(0.05f);*/
		// due to limited/complicated control over histograms titles drawing it is advised to draw titles with TLatex if needed

		// gStyle->SetErrorX(0.); //no horizontal errorbars. I don't want this by default because I can miss empty bins this way

		// gROOT->ForceStyle(); // no need to invoke this
		
		// Enables using transparency of colors, e. g. SetLineColorAlpha(color,alpha)
		// (among other things)
		gStyle->SetCanvasPreferGL(kTRUE);

		gStyle->SetNumberContours(99);

		gReactionColor["pp"]=kBlue+1;                    // #0000cc
		gReactionColor["BeBe"]=kGreen+2;                 // #009900
		gReactionColor["ArSc"]=kOrange+8;                // #ff6633
		gReactionColor["XeLa"]=kViolet;                  // #cc00ff
		gReactionColor["PbPb"]=kRed+1;                   // #cc0000

		gReactionMarker["pp"]=kFullCircle;               // 20
		gReactionMarker["BeBe"]=kFullDiamond;            // 33
		gReactionMarker["ArSc"]=kFullTriangleDown;       // 23
		gReactionMarker["XeLa"]=kFullCross;              // 34
		gReactionMarker["PbPb"]=kFullSquare;             // 21


		// scaling factors for markers, still to be tuned probably
		gMarkerSizes[kFullSquare]=1.0;
		gMarkerSizes[kFullCircle]=1.2;
		gMarkerSizes[kFullTriangleUp]=1.2;
		gMarkerSizes[kFullTriangleDown]=1.2;
		gMarkerSizes[kFullCross]=1.5;
		gMarkerSizes[kFullDoubleDiamond]=1.7;
		gMarkerSizes[kFullDiamond]=1.7;
		gMarkerSizes[kOpenSquare]=1.0;
		gMarkerSizes[kOpenCircle]=1.2;
		gMarkerSizes[kOpenTriangleUp]=1.2;
		gMarkerSizes[kOpenTriangleDown]=1.2;
		gMarkerSizes[kOpenCross]=1.5;
		gMarkerSizes[kOpenDoubleDiamond]=1.7;
		gMarkerSizes[kOpenDiamond]=1.7;

		SetColorsTab10();

		SetBaseTextSize();
		
	}

	/** 
	Setting an alternative color scheme for 1D plots (based on tableu)
	*/
	void UseAlternativeColors(){

		float rgb[6][3]= {{31.,119.,180.},
		                  {254.,127.,14.},
		                  {43.,160.,45.},
		                  {213.,39.,40.},
		                  {148.,102.,191.},
		                  {140.,86.,74.}};

		int c[6];
		int d[6];
		int l[6];

		for(int i=0; i<6; i++) {
			c[i] = TColor::GetFreeColorIndex();
			TColor *color = new TColor(c[i], rgb[i][0]/255., rgb[i][1]/255., rgb[i][2]/255.);

			d[i] = TColor::GetFreeColorIndex();
			TColor *color_dark = new TColor(d[i], 0.8*rgb[i][0]/255., 0.8*rgb[i][1]/255., 0.8*rgb[i][2]/255.);

			l[i] = TColor::GetFreeColorIndex();
			TColor *color_light = new TColor(l[i], min(1.5*rgb[i][0]/255.,1.), min(1.5*rgb[i][1]/255.,1.), min(1.5*rgb[i][2]/255.,1.));
		}

		gColors2[0]=c[0];gColors2[1]=c[1];
		gColors3[0]=c[0];gColors3[1]=c[1];gColors3[2]=c[2];
		gColors4[0]=c[0];gColors4[1]=c[1];gColors4[2]=c[2];gColors4[3]=c[3];
		gColors5[0]=c[0];gColors5[1]=c[1];gColors5[2]=c[2];gColors5[3]=c[3];gColors5[4]=c[4];
		gColors6[0]=c[0];gColors6[1]=c[1];gColors6[2]=c[2];gColors6[3]=c[3];gColors6[4]=c[4];gColors6[5]=c[5];

		gColorsLight2[0]=l[0];gColorsLight2[1]=l[1];
		gColorsLight3[0]=l[0];gColorsLight3[1]=l[1];gColorsLight3[2]=l[2];
		gColorsLight4[0]=l[0];gColorsLight4[1]=l[1];gColorsLight4[2]=l[2];gColorsLight4[3]=l[3];
		gColorsLight5[0]=l[0];gColorsLight5[1]=l[1];gColorsLight5[2]=l[2];gColorsLight5[3]=l[3];gColorsLight5[4]=l[4];
		gColorsLight6[0]=l[0];gColorsLight6[1]=l[1];gColorsLight6[2]=l[2];gColorsLight6[3]=l[3];gColorsLight6[4]=l[4];gColorsLight6[5]=l[5];

		gColorsDark2[0]=d[0];gColorsDark2[1]=d[1];
		gColorsDark3[0]=d[0];gColorsDark3[1]=d[1];gColorsDark3[2]=d[2];
		gColorsDark4[0]=d[0];gColorsDark4[1]=d[1];gColorsDark4[2]=d[2];gColorsDark4[3]=d[3];
		gColorsDark5[0]=d[0];gColorsDark5[1]=d[1];gColorsDark5[2]=d[2];gColorsDark5[3]=d[3];gColorsDark5[4]=d[4];
		gColorsDark6[0]=d[0];gColorsDark6[1]=d[1];gColorsDark6[2]=d[2];gColorsDark6[3]=d[3];gColorsDark6[4]=d[4];gColorsDark6[5]=d[5];

	}

	/** 
	Fills background with a gray grid.
	*/
	void FillBackground(int fill_style=3544, int fill_color=1, float fill_alpha=0.1) {
	        gPad->Update();
		gPad->GetFrame()->SetFillColorAlpha(fill_color,fill_alpha);
		gPad->GetFrame()->SetFillStyle(fill_style);
	}


	/** 
	Draws "preliminary" label on the plot at gives x, y \f$\in [0,1]\f$ at angle (in deg), with alpha transparency \f$\in [0,1]\f$ (default 0.3) and size given in pixels (default 22).
	*/
	void DrawShinePreliminary(float x, float y, float angle=0, float alpha=0.3, int size=22){
		TLatex* tl = new TLatex();
		tl->SetTextFont(gBaseFont);
		tl->SetTextSize(size);
		tl->SetTextAngle(angle); // angle in degrees
		tl->SetTextColorAlpha(kBlack,alpha);
		tl->DrawLatexNDC(x,y,"NA61/SHINE preliminary");
	}

	/**
	Draws an image at given x1, y1, x2, y2 coordinates (\f$\in [0,1]\f$). Path to the file must be given.
	*/
	void DrawImage(float x1, float y1, float x2, float y2, std::string path){

		gStyle->SetFrameFillStyle(0);
		TImage *img = TImage::Open(path.c_str());

		if (!img) {
			printf("Could not create an image... exit\n");
			return;
		}
		TPad *l = new TPad("l","l",x1,y1,x2,y2);
		l->SetBorderMode(0);
		l->SetFillStyle(4000);
		l->Draw();
		l->cd();
		img->Draw();

		Int_t ci = TColor::GetFreeColorIndex();
		TColor *color = new TColor(ci, 0,0,0,"",0.);
		((TFrame*)l->FindObject("TFrame"))->SetLineColor(ci);

	}


	/**
	Draws NA61/SHINE logo at given x1, y1, x2, y2 coordinates (\f$\in [0,1]\f$). Change filepath if neccesary
	*/
	void DrawShineFinal(float x1, float y1, float x2, float y2){
		const string basedir(gSystem->Getenv("SHINYDIR"));
		DrawImage(x1,y1,x2,y2,(basedir+"/NA61-SHINE_H_logo.png").c_str());
	}

	/**
	Draw TLatex with preconfigured style.
	*/
	void DrawLabel(float x, float y, std::string str, int color=kBlack, float size=-1) {

		if(size==-1)
			size=gBaseTextSize;

		static TLatex* tl = new TLatex();
		tl->SetTextFont(gBaseFont);
		tl->SetTextColor(color);
		tl->SetTextSize(size);
		
		tl->DrawLatexNDC(x,y,str.c_str());
	}

	/**
	Draw TLatex with preconfigured style with white edges (like movie subtitiles) 
	*/
	void DrawLabelBlackOnWhite(float x, float y, std::string str, int color=kBlack, float size=-1) {

		if(size==-1)
			size=gBaseTextSize;

		static TLatex* tl = new TLatex();
		tl->SetTextFont(gBaseFont);
		tl->SetTextSize(size);
		tl->SetTextColor(kWhite);

		const int n=30.;

		for(int j=0; j<n; j++) {
			float dx = 0.002*sin(float(j)*2*3.14/float(n));
			float dy = 0.002*cos(float(j)*2*3.14/float(n)) - 0.001;
			tl->DrawLatexNDC(x+dx,y+dy,str.c_str());
		}

		tl->SetTextColor(color);
		tl->DrawLatexNDC(x,y,str.c_str());
	}

	/**
	Draw TLatex with preconfigured style with black edges, white text (like movie subtitiles) 
	*/
	void DrawLabelWhiteOnBlack(float x, float y, std::string str, int color=kBlack, float size=-1) {

		if(size==-1)
			size=gBaseTextSize;

		static TLatex* tl = new TLatex();
		tl->SetTextFont(gBaseFont);
		tl->SetTextSize(size);
		tl->SetTextColor(color);

		const int n=30;
		for(int j=0; j<n; j++) {
			float dx = 0.002*sin(float(j)*2*3.14/float(n));
			float dy = 0.002*cos(float(j)*2*3.14/float(n)) - 0.001;
			tl->DrawLatexNDC(x+dx,y+dy,str.c_str());
		}

		tl->SetTextColor(kWhite);
		tl->DrawLatexNDC(x,y,str.c_str());
	}





	/**
	Setting custom palettes with hardcoded stops and colours defining the gradients. It is meant to be used through SetPalette().
	*/
	int SetHardcodedPalette(const int mapNumber, const int nCont=99, const bool reverse=false) {

		std::vector<Double_t> red;
		std::vector<Double_t> green;
		std::vector<Double_t> blue;
		std::vector<Double_t> stops;

		switch (mapNumber) {
			case 1: {
				/** AntoniStandard
				* Legacy palette used in early publications of NA61 (deprecated)
				*/
				const int steps=10;
				std::vector<Double_t> Red = { 0.90, 0.80, 0.40, 0.00, 0.25, 0.87, 0.90, 1.00, 0.60, 0.31 };
				std::vector<Double_t> Green = { 0.90, 0.80, 0.50, 0.85, 1.00, 1.00, 0.50, 0.20, 0.00, 0.00 };
				std::vector<Double_t> Blue = { 1.00, 1.00, 1.00, 0.85, 0.25, 0.12, 0.05, 0.00, 0.00, 0.00 };
				std::vector<Double_t> Length = { 0.00, 0.02, 0.15, 0.30, 0.44, 0.58, 0.72, 0.86, 0.98, 1.00 };
				red = Red; green = Green; blue = Blue; stops = Length; }
				break;

			case 2: {
				/** DivergingWhite 
				* Diverging Palette for plotting ratios: blue -> white -> red
				*/
				const int steps=5;
				std::vector<Double_t> Red = {0.00, 0.00, 1.00, 1.00, 0.40};
				std::vector<Double_t> Green = {0.00, 0.10, 1.00, 0.10, 0.00};
				std::vector<Double_t> Blue = {0.40, 1.00, 1.00, 0.00, 0.00};
				std::vector<Double_t> Length = {0.00, 0.10, 0.50, 0.90, 1.00};
				red = Red; green = Green; blue = Blue; stops = Length; }
				break;

			case 3: {
				/** DivergingPastel
				* Diverging Palette for plotting ratios in pastel colors.
				*/
				double color1[] = {0, 66, 157};
				double color2[] = {86, 129, 185};
				double color3[] = {147, 196, 210};
				double color4[] = {255, 255, 224};
				double color5[] = {255, 165, 158};
				double color6[] = {221, 76, 101};
				double color7[] = {147, 0, 58};

				for(int i=0; i<3; i++){
					color1[i]/=255.;
					color2[i]/=255.;
					color3[i]/=255.;
					color4[i]/=255.;
					color5[i]/=255.;
					color6[i]/=255.;
					color7[i]/=255.;
				}

				const int steps=7;
				std::vector<Double_t> Red = {color1[0],color2[0],color3[0],color4[0],color5[0],color6[0],color7[0]};
				std::vector<Double_t> Green = {color1[1],color2[1],color3[1],color4[1],color5[1],color6[1],color7[1]};
				std::vector<Double_t> Blue = {color1[2],color2[2],color3[2],color4[2],color5[2],color6[2],color7[2]};
				std::vector<Double_t> Length = {     0.00,     0.16,     0.33,     0.50,     0.66,     0.84,     1.00};
				red = Red; green = Green; blue = Blue; stops = Length; }
				break;

			case 4: {
				/** DivergingSunset
				* Default diverging palette. A merger of "Sunset" and "Blue" palettes.
				*/
				double c1[] = {10, 10, 23, 1};
				double c2[] = {23, 38, 90, 1};
				double c3[] = {29, 58, 120, 1};
				double c4[] = {35, 96, 153, 1};
				double c5[] = {86, 168, 164, 1};

				double c6[] = {250, 244, 234, 1};
				double c7[] = {245., 245., 245.};

				double c8[] = {228., 205., 128.};
				double c9[] = {228., 167.,  55.};
				double c10[]= {224., 127.,  22.};
				double c11[]= {212.,  79.,  16.};
				double c12[]= {173.,  47.,  43.};
				double c13[]= {119.,  30.,  75.};
				double c14[]= {48., 13., 68.};

				for(int i=0; i<3; i++){
					c1[i]/=255.;
					c2[i]/=255.;
					c3[i]/=255.;
					c4[i]/=255.;
					c5[i]/=255.;
					c6[i]/=255.;
					c7[i]/=255.;
					c8[i]/=255.;
					c9[i]/=255.;
					c10[i]/=255.;
					c11[i]/=255.;
					c12[i]/=255.;
					c13[i]/=255.;
					c14[i]/=255.;
				}

				const int steps=11;
				std::vector<Double_t> Red = {c2[0],c3[0],c4[0],c5[0], c6[0] ,c8[0],c9[0],c10[0],c11[0],c12[0],c13[0]};
				std::vector<Double_t> Green = {c2[1],c3[1],c4[1],c5[1], c6[1] ,c8[1],c9[1],c10[1],c11[1],c12[1],c13[1]};
				std::vector<Double_t> Blue = {c2[2],c3[2],c4[2],c5[2], c6[2] ,c8[2],c9[2],c10[2],c11[2],c12[2],c13[2]};
				std::vector<Double_t> Length =  { 0.00, 0.10, 0.20, 0.30,  0.50 , 0.60, 0.65,  0.71,  0.78,  0.90,  1.00};
				red = Red; green = Green; blue = Blue; stops = Length; }
				break;

			case 5: {
				/** SunsetInverted
				* Perceptually uniform palette inverted ROOT kSunset palette.
				*/
				const int steps=9;
				std::vector<Double_t> Red = { 245./255., 228./255., 228./255., 224./255., 212./255., 173./255., 119./255., 48./255., 0./255.};
				std::vector<Double_t> Green = { 245./255., 205./255.,  167./255.,  127./255.,  79./255., 47./255., 30./255., 13./255., 0./255.};
				std::vector<Double_t> Blue = { 245./255., 128./255.,  55./255.,  22./255.,  16./255.,  43./255.,  75./255., 68./255., 0./255.};
				std::vector<Double_t> Length = { 0.0000, 0.1250, 0.2500, 0.3750, 0.5000, 0.6250, 0.7500, 0.8750, 1.0000};
				red = Red; green = Green; blue = Blue; stops = Length; }
				break;

			case 6: {
				/** SunsetInvertedTruncated
				* Perceptually uniform palette inverted ROOT kSunset palette in a truncated version
				* to improve visibility on light end of the palette.
				*/
				const int steps=9;
				std::vector<Double_t> Red = { 238./255., 228./255., 228./255., 224./255., 212./255., 173./255., 119./255., 48./255., 20./255.};
				std::vector<Double_t> Green = { 229./255., 205./255.,  167./255.,  127./255.,  79./255., 47./255., 30./255., 13./255., 5./255.};
				std::vector<Double_t> Blue = { 196./255., 128./255.,  55./255.,  22./255.,  16./255.,  43./255.,  75./255., 68./255., 28./255.};
				std::vector<Double_t> Length = { 0.0000, 0.1000, 0.2400, 0.3750, 0.5000, 0.6250, 0.7600, 0.9000, 1.0000};
				red = Red; green = Green; blue = Blue; stops = Length; }
			break;

			case 7: {
				/** Green
				* Perceptually uniform palette in green tones
				*/
				double color1[] = {250, 244, 234, 1};
				double color2[] = {168, 194, 86, 1};
				double color3[] = {96, 153, 45, 1};
				double color4[] = {41, 90, 23, 1};
				double color5[] = {20, 48, 89, 1};
				double color6[] = {10, 10, 23, 1};

				for(int i=0; i<3; i++){
					color1[i]/=255.;
					color2[i]/=255.;
					color3[i]/=255.;
					color4[i]/=255.;
					color5[i]/=255.;
					color6[i]/=255.;
				}

				const int contours=99;
				const int steps=6;
				static Int_t  colors[contours];
				static Bool_t initialized = kFALSE;
				std::vector<Double_t> Red = {color1[0],color2[0],color3[0],color4[0],color5[0],color6[0]};
				std::vector<Double_t> Green = {color1[1],color2[1],color3[1],color4[1],color5[1],color6[1]};
				std::vector<Double_t> Blue = {color1[2],color2[2],color3[2],color4[2],color5[2],color6[2]};
				std::vector<Double_t> Length = {     0.00,     0.15,     0.32,     0.55,     0.80,     1.00};
				red = Red; green = Green; blue = Blue; stops = Length; }
			break;

			case 8: {	
				/** Blue
				* Perceptually uniform palette in blue tones
				*/
				double color1[] = {234, 244, 250, 1};
				double color2[] = {86, 168, 164, 1};
				double color3[] = {35, 96, 153, 1};
				double color4[] = {29, 58, 120, 1};
				double color5[] = {23, 38, 90, 1};
				double color6[] = {10, 10, 23, 1};

				for(int i=0; i<3; i++){
					color1[i]/=255.;
					color2[i]/=255.;
					color3[i]/=255.;
					color4[i]/=255.;
					color5[i]/=255.;
					color6[i]/=255.;
				}

				const int contours=99;
				const int steps=6;
				static Int_t  colors[contours];
				static Bool_t initialized = kFALSE;
				std::vector<Double_t> Red = {color1[0],color2[0],color3[0],color4[0],color5[0],color6[0]};
				std::vector<Double_t> Green = {color1[1],color2[1],color3[1],color4[1],color5[1],color6[1]};
				std::vector<Double_t> Blue = {color1[2],color2[2],color3[2],color4[2],color5[2],color6[2]};
				std::vector<Double_t> Length = {     0.00,     0.12,     0.25,     0.45,     0.80,     1.00};
				red = Red; green = Green; blue = Blue; stops = Length; }
			break;
		}

		if (reverse) {
			std::reverse(red.begin(), red.end());
			std::reverse(green.begin(), green.end());
			std::reverse(blue.begin(), blue.end());
		}
		return TColor::CreateGradientColorTable(stops.size(),
				&stops.front(),
				&red.front(),
				&green.front(),
				&blue.front(),
				nCont);
	}


	/**
	Sets chosen colormap. The full list of palettes can be viewed in CET directory and on top of that we have defined a number of custom palettes, including: "AntoniStandard","DivergingWhite","DivergingPastel","DivergingSunset","SunsetInverted","SunsetInvertedTruncated","Green","Blue".
	*/
	int SetPalette(const string& mapName, int nCont=-1, const bool reverse=false)
	{

		// nCont for divergent palettes by defult 21
		// (odd to have a one color for neutral value and low to better distinguish between the values)
		// for others let's take ROOT's maximum
		if( nCont==-1 )
			nCont=99;
		else if ( ((mapName.find("diverging") != std::string::npos) || (mapName.find("Diverging") != std::string::npos)) \
		     && nCont==-1 )
			nCont=21;

		// this part loads the color palettes defined ROOT-style in the code above
		static std::map<string,int> palette_numbering;
		palette_numbering["AntoniStandard"] = 1;
		palette_numbering["DivergingWhite"] = 2;
		palette_numbering["DivergingPastel"] = 3;
		palette_numbering["DivergingSunset"] = 4;
		palette_numbering["SunsetInverted"] = 5;
		palette_numbering["SunsetInvertedTruncated"] = 6;
		palette_numbering["Green"] = 7;
		palette_numbering["Blue"] = 8;

		if (palette_numbering.count(mapName)) {
			int mapNumber = palette_numbering[mapName];
			SetHardcodedPalette(mapNumber, nCont, reverse);
			return 0;
		}

		// the rest of the code reads the palettes from the xml files in CET directory

		vector<string> configFilenames;
		const string basedir(gSystem->Getenv("SHINYDIR"));
		configFilenames.push_back(basedir + "CET/CETperceptual.xml");
		//configFilenames.push_back(basedir + "CET/All_mpl_cmaps.xml");
		configFilenames.push_back(basedir + "CET/All_mpl3.3.1_cmaps.xml");
		configFilenames.push_back(basedir + "CET/All_idl_cmaps.xml");
		try {
			vector<double> stops;
			vector<double> red;
			vector<double> green;
			vector<double> blue;
			for (const auto configFilename : configFilenames) {
				ptree mainTree;
				read_xml(configFilename, mainTree);
				ptree& mapTree = mainTree.get_child("ColorMaps");
				for(const ptree::value_type& v : mapTree) {
					if (v.first == "ColorMap") {
						const string name = v.second.get<string>("<xmlattr>.name");
						if (name == mapName) {
							for (const ptree::value_type& vv : v.second) {
								if (vv.first == "Point") {
									stops.push_back(vv.second.get<double>("<xmlattr>.x"));
									red.push_back(vv.second.get<double>("<xmlattr>.r"));
									green.push_back(vv.second.get<double>("<xmlattr>.g"));
									blue.push_back(vv.second.get<double>("<xmlattr>.b"));
								}
							}
							break;
						}
						else if (mapName == "help") {
							cout << name << endl;
						}
					}
					if (!stops.empty())
						break;
				}
			}

			if (mapName == "help" || stops.empty()) {
				ifstream custom(basedir + "CET/All_custom.txt");
				string name;
				string line;

				while (getline(custom, line)) {
					if (line[0] == '#') {
						const string currName = trim_copy(line.substr(1, line.size() - 1));
						if (mapName == "help") {
							cout << currName << endl;
						}
						else if (name == mapName) {
							if (stops.empty())
								std::cout<<"ERROR: emtpy custom color"<<std::endl;
							break;
						}
						name = currName;
					}
					else {
						if (name == mapName) {
							istringstream tmp(line);
							double s, r, g, b;
							tmp >> s >> r >> g >> b;
							stops.push_back(s);
							red.push_back(r/255.);
							green.push_back(g/255.);
							blue.push_back(b/255.);
						}
					}
				}
			}

			if (mapName == "help")
				return 0;
			else {
				if (stops.empty()) {
					std::cout<<"Warning: could not read " + mapName<<std::endl;
					return -1;
				}
				// idl from [-1, 1]
				if (stops[0] == -1) {
					for (auto& s : stops)
						s = (s + 1) / 2.;
				}
				if (reverse) {
					std::reverse(red.begin(), red.end());
					std::reverse(green.begin(), green.end());
					std::reverse(blue.begin(), blue.end());
				}
				return TColor::CreateGradientColorTable(stops.size(),
						&stops.front(),
						&red.front(),
						&green.front(),
						&blue.front(),
						nCont);
			}
		}
		catch(boost::exception const& ex) {
			std::cout<<"ERROR: error reading colors \n" + boost::diagnostic_information(ex)<<std::endl;
		}
		return 0;
	}



	/** Setting basic style properties for 2D plots
	*/
	void SetStyle2D() {
		gStyle->SetTitleOffset(2.1, "x");
		gStyle->SetTitleOffset(2.1, "y");
		gStyle->SetTitleOffset(2.1, "z");
		//gStyle->SetLabelOffset(0.015, "xy");
		gStyle->SetPadTopMargin(0.06);
		gStyle->SetPadBottomMargin(0.16);
		gStyle->SetPadLeftMargin(0.20);
		gStyle->SetPadRightMargin(0.20);
		gStyle->SetNdivisions(505,"xyz");
		SetPalette("SunsetInverted",99,false);
	}

	/** Setting basic style properties for 1D plots
	*/
	void SetStyle1D() {
		gStyle->SetLabelSize(20, "xy");
		gStyle->SetTitleSize(25, "xy");
		gStyle->SetTitleOffset(1.1, "x");
		gStyle->SetTitleOffset(1.0, "y");
		gStyle->SetLabelOffset(0.01, "xy");
		gStyle->SetPadTopMargin(0.06);
		gStyle->SetPadBottomMargin(0.15);
		gStyle->SetPadLeftMargin(0.13);
		gStyle->SetPadRightMargin(0.04);
		gStyle->SetNdivisions(505,"xyz");
	}



} // End namespace ShinyStyle

/*! @} End of Doxygen Groups*/

